package com.asu.studentdatabase;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class DataAdapter extends BaseAdapter {
    Context context;
    ArrayList<Name_data> name_data;
    LayoutInflater layoutInflater;
    Activity activity;
    public DataAdapter(Context context, ArrayList<Name_data> name_data, Activity activity) {
        this.context=context;
        this.name_data=name_data;
        this.activity=activity;
        layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return name_data.size();
    }

    @Override
    public Object getItem(int i) {
        return name_data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        TextView txt_name;
        LinearLayout linear;
        view=layoutInflater.inflate(R.layout.name_layout,null);
        txt_name = view.findViewById(R.id.txt_name);
        linear = view.findViewById(R.id.linear);
        txt_name.setText(name_data.get(i).getName());
        linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,Info.class);
                intent.putExtra("name",name_data.get(i).getName());
                context.startActivity(intent);
            }
        });
        return view;
    }
}
