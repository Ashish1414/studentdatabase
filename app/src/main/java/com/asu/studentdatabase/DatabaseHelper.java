package com.asu.studentdatabase;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Paint;

public class DatabaseHelper  extends SQLiteOpenHelper {
    public static final String DATABASE_NAME="STUDENT.db";
    public static final String TABLE_NAME="Info_table";
    public static final String COL_1="Name";
    public static final String COL_2="School";
    public static final String COL_3="Address";
    public static final String COL_4="Self_Contact";
    public static final String COL_5="Parent_Contact";
    public static final String COL_6="Email";
    public static final String COL_7="Enquiry";
    public static final String COL_8="Interest";
    public static final String COL_9="Dream";
    public static final String COL_10="Refer";
    public static final String COL_11="Joining";
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE "+TABLE_NAME+" (NAME Text ,SCHOOL TEXT,ADDRESS TEXT,SELF_CONTACT Text,PARENT_CONTACT Text, EMAIL Text, ENQUIRY Text, INTEREST Text, DREAM Text, REFER Text, JOINING Text  )");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(db);

    }

    public boolean insertData(String Name,String School, String Address, String Self, String Parent, String Email, String Enquiry, String interest, String Dream, String refer, String joining)
    {
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_1,Name);
        contentValues.put(COL_2,School);
        contentValues.put(COL_3,Address);
        contentValues.put(COL_4,Self);
        contentValues.put(COL_5,Parent);
        contentValues.put(COL_6,Email);
        contentValues.put(COL_7,Enquiry);
        contentValues.put(COL_8, interest);
        contentValues.put(COL_9,Dream);
        contentValues.put(COL_10,refer);
        contentValues.put(COL_11,joining);
        long result = db.insert(TABLE_NAME,null,contentValues);
        if (result == -1)
        {
            return false;
        }
        else {
            return true;
        }
    }

    public Cursor getAllData(String Name)
    {
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM "+TABLE_NAME+" WHERE NAME="+"\""+Name+"\"",null);
        return res;
    }

    public Cursor getName()
    {
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor res = db.rawQuery("SELECT NAME FROM "+TABLE_NAME,null);
        return res;
    }
    public Integer deleteData(String id)
    {
        SQLiteDatabase db=this.getWritableDatabase();
        return db.delete(TABLE_NAME , "ID = ?",new String[]{id});
    }
}

