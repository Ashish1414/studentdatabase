package com.asu.studentdatabase;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Info extends AppCompatActivity {
    DatabaseHelper myDb;
    TextView txtname,txtschool,txtaddress,txtphone,txtparent,txtmail,txtenq,txtinterest,txtdream,txtrefer,txtjoining;
    String names,school,address,phone,parent,email,enquiry,interest,dream,refer,joining;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        myDb = new  DatabaseHelper(this);
        Intent intent = getIntent();
        Bundle bundle =intent.getExtras();
        String name = bundle.getString("name");
        Cursor res =  myDb.getAllData(name);
        while (res.moveToNext()) {
            names = res.getString(0);
            school = res.getString(1);
            address = res.getString(2);
            phone = res.getString(3);
            parent = res.getString(4);
            email = res.getString(5);
            enquiry = res.getString(6);
            interest = res.getString(7);
            dream = res.getString(8);
            refer = res.getString(9);
            joining = res.getString(10);
        }
        txtname = findViewById(R.id.txtname);
        txtschool=findViewById(R.id.txtschool);
        txtaddress=findViewById(R.id.txtaddress);
        txtphone=findViewById(R.id.txtphone);
        txtparent=findViewById(R.id.txtparent);
        txtenq=findViewById(R.id.txtenq);
        txtinterest=findViewById(R.id.txtinterest);
        txtdream=findViewById(R.id.txtdream);
        txtmail=findViewById(R.id.txtmail);
        txtrefer=findViewById(R.id.txtrefer);
        txtjoining=findViewById(R.id.txtjoining);
        txtname.setText(names);
        txtschool.setText(school);
        txtaddress.setText(address);
        txtphone.setText(phone);
        txtparent.setText(parent);
        txtmail.setText(email);
        txtenq.setText(enquiry);
        txtinterest.setText(interest);
        txtdream.setText(dream);
        txtrefer.setText(refer);
        txtphone.setText(phone);
        txtjoining.setText(joining);

        txtphone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(Info.this, new String[]{Manifest.permission.CALL_PHONE}, 1);
                Intent i = new Intent(Intent.ACTION_CALL);
                i.setData(Uri.parse("tel:"+txtphone.getText().toString()));
                startActivity(i);
            }
        });



    }
}
