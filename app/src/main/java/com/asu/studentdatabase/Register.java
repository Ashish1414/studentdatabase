package com.asu.studentdatabase;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

public class Register extends AppCompatActivity {
    DatabaseHelper myDb;
    DatePicker joining;
    EditText edt_name,edt_school,edt_add,edt_phone,edt_parent,edt_mail,edt_enq,edt_interest,edt_dream,edt_refer;
    String join_str;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        myDb = new DatabaseHelper(this);
        joining = findViewById(R.id.joining);
        edt_name = findViewById(R.id.edt_name);
        edt_school = findViewById(R.id.edt_school);
        edt_add = findViewById(R.id.edt_add);
        edt_phone = findViewById(R.id.edt_number);
        edt_parent = findViewById(R.id.edt_parent);
        edt_mail = findViewById(R.id.edt_mail);
        edt_enq = findViewById(R.id.edt_enq);
        edt_interest = findViewById(R.id.edt_interest);
        edt_dream = findViewById(R.id.edt_dream);
        edt_refer = findViewById(R.id.edt_refer);

        findViewById(R.id.btnRegister).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int d=joining.getDayOfMonth();
                int mon=joining.getMonth()+1;
                join_str = joining.getYear() + "-" + mon + "-" + d;
                myDb.insertData(edt_name.getText().toString(),edt_school.getText().toString(),edt_add.getText().toString()
                        ,edt_phone.getText().toString(),edt_parent.getText().toString(),edt_mail.getText().toString(),edt_enq.getText().toString()
                        ,edt_interest.getText().toString(),edt_dream.getText().toString(),edt_refer.getText().toString(),join_str);
                Toast.makeText(Register.this, "Registered Successfully", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Register.this , MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
