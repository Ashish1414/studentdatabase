package com.asu.studentdatabase;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class ViewStudent extends AppCompatActivity {
    DatabaseHelper myDb;
    ArrayList<Name_data> name_data;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);
        myDb = new DatabaseHelper(this);
        listView = findViewById(R.id.list_view);
        Cursor res =  myDb.getName();
        name_data = new ArrayList<>();
        while (res.moveToNext())
        {
            name_data.add(new Name_data(res.getString(0)));

        }
        DataAdapter adapter = new DataAdapter(this,name_data,this);
        listView.setAdapter(adapter);
    }
}
